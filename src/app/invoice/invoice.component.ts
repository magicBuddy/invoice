import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import * as $ from 'jquery';
import {HttpClient} from '@angular/common/http';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import cities from '../../assets/usa-cities.json';

@Component({
  selector: "app-invoice",
  templateUrl: "./invoice.component.html",
  styleUrls: ["./invoice.component.css"],
})
export class InvoiceComponent implements OnInit {
  name = "";

  @ViewChild("content", { static: false }) content: ElementRef;
  private url = "http://localhost:3000/cities";
  invoiceIssuerName: String = "";
  invoiceIssuerCompanyName: String = "";
  invoiceIssuerStreetAddress: String = "";
  invoiceIssuerCity: String = "";
  invoiceIssuerWeeks: String = "";
  issuedDate: String = "";
  startDate: String = "";
  endDate: String = "";
  movieDetails: Array<any> = [];
  movieOrigin: String = "";
  movieDestination: String = "";
  movieDate: String = "";
  movieRate: String = "";

  dateOfFuel: String = "";
  locationOfFuel: String = "";
  totalAmountOfFuel: String = "";

  products: any = [];
  filterone: any = [];
  movies: Array<String> = [];
  moviesTable: Array<String> = [];
  fuels: Array<any> = [];
  adjustments: Array<String> = [];
  fuelTable: Array<String> = [];
  keyword = "name";
  finalFreightSum = 0;
  totalFuelSum = 0;
  insuranceAmount = "";
  accessoriesAmount = "";
  modelablcok: any = [0];
  modelabl: any = [0];
  grandtotal: 0;
  model: any;
  private isButtonVisible = true;

  selectEvent(item: string) {}

  onChangeSearch(val: string) {}

  onFocused(e: string) {}

  onKeyUpForName(event: any) {
    this.invoiceIssuerName = event.target.value;
    
  }
  onKeyUpForCompanyName(event: any) {
    this.invoiceIssuerCompanyName = event.target.value;
  }
  onKeyUpForStreetAddress(event: any) {
    this.invoiceIssuerStreetAddress = event.target.value;
  }
  onKeyUpForCity(event: any) {
    this.invoiceIssuerCity = event.target.value;
  }
  onKeyUpForWeeks(event: any) {
    this.invoiceIssuerWeeks = event.target.value;
  }
  onChangeIssuedDate(event: any) {
    this.issuedDate = event.target.value;
  }
  onChangeStartDate(event: any) {
    this.startDate = event.target.value;
  }
  onChangeEndDate(event: any) {
    this.endDate = event.target.value;
  }

  constructor(private httpClient: HttpClient) {}

  ngOnInit() {
    this.insuranceAmount = (localStorage.getItem("insuranceAmount") ? (localStorage.getItem("insuranceAmount")) : '0')
    this.accessoriesAmount = (localStorage.getItem("accessoriesAmount") ? (localStorage.getItem("accessoriesAmount")) : '0')
    
    this.movieDetails = localStorage.getItem("movie-details")
      ? JSON.parse(localStorage.getItem("movie-details")).filter(
          (movie) => movie.movieOrigin !== "" || movie.movieDestination !== ""
        )
      : [];
    this.fuels = localStorage.getItem("fuel-details")
      ? JSON.parse(localStorage.getItem("fuel-details")).filter(
          (fuel) =>
            fuel.dateOfFuel !== "" ||
            fuel.locationOfFuel !== "" ||
            fuel.totalAmountOfFuel !== ""
        )
      : [];

    if (
      localStorage.getItem("name") &&
      localStorage.getItem("name").trim().length > 0
    ) {
      this.invoiceIssuerName = localStorage.getItem("name");
    }
    if (
      localStorage.getItem("company-name") &&
      localStorage.getItem("company-name").trim().length > 0
    ) {
      this.invoiceIssuerCompanyName = localStorage.getItem("company-name");
    }
    if (
      localStorage.getItem("street-address") &&
      localStorage.getItem("street-address").trim().length > 0
    ) {
      this.invoiceIssuerStreetAddress = localStorage.getItem("street-address");
    }
    if (
      localStorage.getItem("city") &&
      localStorage.getItem("city").trim().length > 0
    ) {
      this.invoiceIssuerCity = localStorage.getItem("city");
    }
    if (
      localStorage.getItem("statement-weeks") &&
      localStorage.getItem("statement-weeks").trim().length > 0
    ) {
      this.invoiceIssuerWeeks = localStorage.getItem("statement-weeks");
    }
    if (
      localStorage.getItem("issued-date") &&
      localStorage.getItem("issued-date").trim().length > 0
    ) {
      this.issuedDate = localStorage.getItem("issued-date");
    }
    if (
      localStorage.getItem("start-date") &&
      localStorage.getItem("start-date").trim().length > 0
    ) {
      this.startDate = localStorage.getItem("start-date");
    }
    if (
      localStorage.getItem("end-date") &&
      localStorage.getItem("end-date").trim().length > 0
    ) {
      this.endDate = localStorage.getItem("end-date");
    }

    const formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
    });

    $(document).ready(() => {
      $("#grandtotal").html(formatter.format(localStorage.getItem("grandTotal") ? parseFloat(localStorage.getItem("grandTotal")) : 0));
      $("#duetotal").html(formatter.format(localStorage.getItem("totalDue") ? parseFloat(localStorage.getItem("totalDue")) : 0))
      $("#result").html(formatter.format(localStorage.getItem("result") ? parseFloat(localStorage.getItem("result")) : 0))
      $("#total_sum_value").html(formatter.format(localStorage.getItem("freightCharges") ? parseFloat(localStorage.getItem("freightCharges")) : 0))
      $("#fueltotal").html(formatter.format(localStorage.getItem("fuelTotal") ? parseFloat(localStorage.getItem("fuelTotal")) : 0))
      
      $("#myTable").on("input", ".txtCal", function () {
        let calculatedTotalSum = 0;
        $("#myTable .txtCal").each(function () {
          const getTextboxValue = $(this).val();
          if ($.isNumeric(getTextboxValue)) {
            calculatedTotalSum += parseFloat(getTextboxValue);
          }
        });

        $("#total_sum_value").html(formatter.format(calculatedTotalSum));
        const main = calculatedTotalSum;
        const disc = 10;
        const dec = disc / 100; // its convert 10 into 0.10
        const mult = main * dec;
        $("#result").html(formatter.format(mult));

        const finalamount = calculatedTotalSum;
        this.finalFreightSum = finalamount - mult;
        $("#duetotal").html(formatter.format(this.finalFreightSum));
        const $this = $(this);

        $("#adjusttotal").click();
        $("#fueltotal").click();
        $("#duetotal").click();
      });

      $("#AddjectTable").on("input", ".qty1", () => {
        let sum = 0;
        $(".qty1").each(function () {
          sum += +$(this).val();
        });
        $("#adjusttotal").click();
        $("#fueltotal").click();
        $("#duetotal").click();
      });

      $(document).ready(() => {
        $("#fuelTable").on("input", ".fuelCal", function () {
          let fuelTotal = 0;
          $("#fuelTable .fuelCal").each(function () {
            const getFuelVal = $(this).val();
            if ($.isNumeric(getFuelVal)) {
              fuelTotal += parseFloat(getFuelVal);
            }
          });

          this.totalFuelSum = fuelTotal;
          $("#fueltotal").html(formatter.format(fuelTotal));
          $("#adjusttotal").click();
          $("#fueltotal").click();
          $("#duetotal").click();
        });
      });
    });

    $(document).ready(() => {
      $(".main").on("click", ".grandtotal", () => {
        let grandTotal = 0;
        $(".main .grandtotal").each(function () {
          const getGrandTotalValue = $(this).val();
          if ($.isNumeric(getGrandTotalValue)) {
            grandTotal += parseFloat(getGrandTotalValue);
          }
        });
        $("#grand_total").html(grandTotal);
      });
    });

    this.products = cities;

    /*    this.httpClient.get(this.url).subscribe(data => {
          this.products = data;

        });*/
  }

  searchblock = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map((term) =>
        term === ""
          ? []
          : this.products
              .filter((v) => v.toLowerCase().indexOf(term.toLowerCase()) > -1)
              .slice(0, 15)
      )
    );
  formatters = (x) => x;

  onMovieOriginSelectItem($event: any, input: any): void {
    event.preventDefault();
    this.movieOrigin = $event.item;
    const viewlist = $event;
    input.value = "";
  }
  onMovieDestinationSelectItem($event, input): void {
    event.preventDefault();
    this.movieDestination = $event.item;
    const viewlist = $event;
    input.value = "";
  }
  onChangeMovieDate(event: any) {
    this.movieDate = event.target.value;
  }
  onKeyUpForMovieRate(event: any) {
    this.movieRate = event.target.value;
  }

  trackByIndex(index: number, obj: any): any {
    return index;
  }

  addElement() {
    if(this.movieDetails.findIndex(movie=> movie.movieOrigin.length === 0 || movie.movieDestination.length===0 || movie.movieDate.length === 0 || movie.movieRate.length === 0) !== -1 && (this.movieOrigin.trim().length ===0 || this.movieDestination.trim().length===0 || this.movieRate.trim().length===0 || this.movieDate.trim().length === 0))
    {
      // alert("Please complete the added row before adding new one...");
      return;
    }
    if (
      this.movieDetails.length >= 1 &&
      this.movieOrigin.length === 0 &&
      this.movieDestination.length === 0 &&
      this.movieDate.length === 0 &&
      this.movieRate.length === 0
    ) {
      this.movieDetails.push({
        movieOrigin: "",
        movieDestination: "",
        movieDate: "",
        movieRate: "",
      });
    } else if (this.movieDetails.length >= 1) {
      this.movieDetails[this.movieDetails.length - 1] = {
        movieOrigin: this.movieOrigin,
        movieDestination: this.movieDestination,
        movieDate: this.movieDate,
        movieRate: this.movieRate,
      };
      this.movieDetails.push({
        movieOrigin: "",
        movieDestination: "",
        movieDate: "",
        movieRate: "",
      });
    } else if (this.movieDetails.length === 0) {
      this.movieDetails.push({
        movieOrigin: "",
        movieDestination: "",
        movieDate: "",
        movieRate: "",
      });
    }

    this.movieOrigin = "";
    this.movieDestination = "";
    this.movieDate = "";
    this.movieRate = "";
  }

  onChangeFuelDate(event: any) {
    this.dateOfFuel = event.target.value;
  }
  onKeyUpForFuelLocation(event: any) {
    this.locationOfFuel = event.target.value;
  }
  onKeyUpForFuelAmount(event: any) {
    this.totalAmountOfFuel = event.target.value;
  }

  addFuel() {
    if (this.fuels.findIndex(fuel => fuel.totalAmountOfFuel.length === 0 || fuel.dateOfFuel.length === 0 || fuel.locationOfFuel.length === 0) !== -1 && (this.totalAmountOfFuel.trim().length === 0 || this.locationOfFuel.trim().length === 0 || this.dateOfFuel.trim().length === 0)) {
      // alert("Please complete the added row before adding new one...");
      return;
    }

    if (
      this.fuels.length >= 1 &&
      this.totalAmountOfFuel.length === 0 &&
      this.dateOfFuel.length === 0 &&
      this.locationOfFuel.length === 0
    ) {
      this.fuels.push({
        totalAmountOfFuel: "",
        dateOfFuel: "",
        locationOfFuel: "",
      });
    } else if (this.fuels.length >= 1) {
      this.fuels[this.fuels.length - 1] = {
        totalAmountOfFuel: this.totalAmountOfFuel,
        dateOfFuel: this.dateOfFuel,
        locationOfFuel: this.locationOfFuel,
      };
      this.fuels.push({
        totalAmountOfFuel: "",
        dateOfFuel: "",
        locationOfFuel: "",
      });
    } else if (this.fuels.length === 0) {
      this.fuels.push({
        totalAmountOfFuel: "",
        dateOfFuel: "",
        locationOfFuel: "",
      });
    }

    this.totalAmountOfFuel = "";
    this.dateOfFuel = "";
    this.locationOfFuel = "";
  }

  onKeyUpForInsuranceAmount(event: any) {
    this.insuranceAmount = event.target.value;
  }
  onKeyUpForAccessoryAmount(event: any) {
    this.accessoriesAmount = event.target.value;
  }
  addAdjustment() {
    this.adjustments.push("Element " + (this.adjustments.length + 1));
  }

  ionViewDidLoad() {
    const formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
    });

    $("input").css("border", "none");
    $(".export_btn").css("display", "none");
    $(".add_btn").css("display", "none");
    $(".add_fuel_btn").css("display", "none");

    let totalDue = $("#duetotal")[0].innerHTML;
    totalDue = totalDue.replace(",", "");
    totalDue = parseFloat(totalDue.substring(1));

    let totalFuel = $("#fueltotal")[0].innerHTML;
    totalFuel = totalFuel.replace(",", "");
    totalFuel = parseFloat(totalFuel.substring(1));

    this.insuranceAmount = parseFloat($("#insurance").val()).toString();
    this.accessoriesAmount = parseFloat($("#access").val()).toString();

    const grandTotal =
      totalDue -
      (totalFuel +
        parseFloat(this.insuranceAmount) +
        parseFloat(this.accessoriesAmount));
    $("#grandtotal").html(formatter.format(grandTotal));

    setTimeout(() => {
      this.exporttopdf();
    }, 1000);
  }

  onSaveData() {
    // if(this.movieDetails.findIndex(movie=> movie.movieOrigin.length !== 0 ) !== -1 && (this.movieOrigin.trim().length ===0 || this.movieDestination.trim().length===0 || this.movieRate.trim().length===0 || this.movieDate.trim().length === 0)){
    //   alert('Please complete the added row before saving your data...')
    //   return;
    // }
    // if(this.totalAmountOfFuel.trim().length ===0 || this.locationOfFuel.trim().length===0 || this.dateOfFuel.trim().length===0){
    //   alert('Please complete the added row before saving your data...')
    //   return;
    // }
    const formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
    });
    
    if (this.movieDetails.length >= 1) {
      if (
        this.movieOrigin.length > 0 &&
        this.movieDestination.length > 0 &&
        this.movieDate.length > 0 &&
        this.movieRate.length > 0
      ) {
        this.movieDetails[this.movieDetails.length - 1] = {
          movieOrigin: this.movieOrigin,
          movieDestination: this.movieDestination,
          movieDate: this.movieDate,
          movieRate: this.movieRate,
        };
      }
      let localMovieDetailsData = [
        ...this.movieDetails.filter(
          (movie) =>
            movie.movieOrigin.length !== 0 ||
            movie.movieDestination.length !== 0 ||
            movie.movieDate.length !== 0 ||
            movie.movieRate.length !== 0
        ),
      ];

      localStorage.setItem(
        "movie-details",
        JSON.stringify(localMovieDetailsData)
      );
    }

    if (this.fuels.length >= 1) {
      if (
        this.totalAmountOfFuel.length > 0 &&
        this.locationOfFuel.length > 0 &&
        this.dateOfFuel.length > 0
      ) {
        this.fuels[this.fuels.length - 1] = {
          totalAmountOfFuel: this.totalAmountOfFuel,
          locationOfFuel: this.locationOfFuel,
          dateOfFuel: this.dateOfFuel,
        };
      }

      let localFuelsData = [
        ...this.fuels.filter(
          (fuel) =>
            fuel.dateOfFuel.length !== 0 ||
            fuel.locationOfFuel.length !== 0 ||
            fuel.totalAmountOfFuel.length !== 0
        ),
      ];
      localStorage.setItem("fuel-details", JSON.stringify(localFuelsData));
    }

    localStorage.setItem("name", this.invoiceIssuerName.toString() || "");
    localStorage.setItem(
      "company-name",
      this.invoiceIssuerCompanyName.toString() || ""
    );
    localStorage.setItem(
      "street-address",
      this.invoiceIssuerStreetAddress.toString() || ""
    );
    localStorage.setItem("city", this.invoiceIssuerCity.toString() || "");
    localStorage.setItem(
      "statement-weeks",
      this.invoiceIssuerWeeks.toString() || ""
    );
    localStorage.setItem("start-date", this.startDate.toString());
    localStorage.setItem("end-date", this.endDate.toString());
    localStorage.setItem("issued-date", this.issuedDate.toString());
    // localStorage.setItem("insurance-amount", this.insuranceAmount.toString());
    // localStorage.setItem("accessory-amount", this.accessoriesAmount.toString());

    let totalDue = $("#duetotal")[0].innerHTML;
    totalDue = totalDue.replace(",", "");
    totalDue = parseFloat(totalDue.substring(1));
    localStorage.setItem("totalDue", totalDue);

    let freightCharges = $("#total_sum_value")[0].innerHTML;
    freightCharges = freightCharges.replace(",", "");
    freightCharges = parseFloat(freightCharges.substring(1));
    localStorage.setItem("freightCharges", freightCharges);

    let result = $("#result")[0].innerHTML;
    result = result.replace(",", "");
    result = parseFloat(result.substring(1));
    localStorage.setItem("result", result);

    let totalFuel = $("#fueltotal")[0].innerHTML;
    totalFuel = totalFuel.replace(",", "");
    totalFuel = parseFloat(totalFuel.substring(1));
    localStorage.setItem("fuelTotal", totalFuel);

    this.insuranceAmount = parseFloat($("#insurance").val()).toString();
    localStorage.setItem("insuranceAmount", this.insuranceAmount);
    this.accessoriesAmount = parseFloat($("#access").val()).toString();
    localStorage.setItem("accessoriesAmount", this.accessoriesAmount);
    
    const grandTotal =
      totalDue -
      (totalFuel +
        parseFloat(this.insuranceAmount) +
        parseFloat(this.accessoriesAmount));
    $("#grandtotal").html(formatter.format(grandTotal));
    localStorage.setItem("grandTotal", grandTotal.toString());
  }
  exporttopdf() {
    window.print();

    $(".export_btn").css("display", "block");
    $(".add_btn").css("display", "block");
    $(".add_fuel_btn").css("display", "block");
  }
}
